//
//  EditViewController.swift
//  ECE564HW
//
//  Created by Loaner on 9/19/22.
//

import UIKit

class EditViewController: UIViewController {

    
    var baseView: UIView!
    
    var editButton: UIButton!
    var cancelButton: UIButton!
    var clearButton: UIButton!
    
    var firstNameTextField: UITextField!
    var lastNameTextField: UITextField!
    var hometownTextField: UITextField!
    var hobbyTextField: UITextField!
    var netIDTextField: UITextField!
    var bestProgLangTextField: UITextField!
    var departmentTextField: UITextField!
    var infoText: UILabel!
    var teamNameTextField: UITextField!
    
    var genderSelector: UISegmentedControl!
    var roleSelector: UIPickerView!
    var selectedGender: Gender = .Unknown
    let roles = ["TA", "Professor", "Student", "Other"]
    var roleSelected: Role = .Other
    
    var hobbyList: [String] = []
    var programList: [String] = []
    
    var logo: UIImageView!
    var avatar: UIImageView!
    
    var editedPerson = DukePerson()
    var shouldSave = false
    

    override func viewDidLoad() {
        let labelMargin:Int = 10
        let textMargin:Int = 180
        let boxHeight:Int = 35
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        baseView = self.view
        baseView.backgroundColor = UIColor(red: 0.900, green: 0.900, blue: 1.0, alpha: 1.0)
        
        //UI SegmentContol Setup
        createGenderSelector(x_coord: 45, y_coord: 515, width: 300, height: 50)
        
        
        //UIpickerView Setup
        createRolePicker(x_coord: textMargin - 10, y_coord: 465, width: 220, height: boxHeight + 10)
       
        //Button Setup
        editButton = placeButton(name: "Update", x_coord: 45, y_coord: 570, width: 95, height: 30)
        cancelButton = placeButton(name: "Cancel", x_coord: 245, y_coord: 570, width: 95, height: 30)
        clearButton = placeButton(name: "Clear", x_coord: 145, y_coord: 570, width: 95, height: 30)
        
        editButton.addTarget(self, action: #selector(EditViewController.editButtonAction(_:)), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(EditViewController.cancelButtonAction(_:)), for: .touchUpInside)
        clearButton.addTarget(self, action: #selector(EditViewController.clearButtonAction(_:)), for: .touchUpInside)
        
        //Text Field Setup
        firstNameTextField =  placeTextField(exampleText: " ex. Jane", x_coord: textMargin, y_coord: 150, width: 200, height: boxHeight)
        lastNameTextField =  placeTextField(exampleText: " ex. Doe", x_coord: textMargin, y_coord: 190, width: 200, height: boxHeight)
        hometownTextField =  placeTextField(exampleText: " ex. Durham,NC", x_coord: textMargin, y_coord: 230, width: 200, height: boxHeight)
        hobbyTextField = placeTextField(exampleText: " ex. Hiking, Biking, Golfing", x_coord: textMargin, y_coord: 270, width: 200, height: boxHeight)
        netIDTextField = placeTextField(exampleText: " ex. jd123", x_coord: textMargin, y_coord: 310, width: 200, height: boxHeight)
        bestProgLangTextField = placeTextField(exampleText: " ex. Python, C, C++", x_coord: textMargin, y_coord: 350, width: 200, height: boxHeight)
        departmentTextField = placeTextField(exampleText: " ex. ECE", x_coord: textMargin, y_coord: 390, width: 200, height: boxHeight)
        teamNameTextField = placeTextField(exampleText: " ex. Team ECE", x_coord: textMargin, y_coord: 430, width: 200, height: boxHeight)
        
        //Label Setup
        placeLabel(inputText: " First Name:", x_coord: labelMargin, y_coord: 150, width: 100, height: boxHeight)
        placeLabel(inputText: " Last Name:", x_coord: labelMargin, y_coord: 190, width: 100, height: boxHeight)
        placeLabel(inputText: " From:", x_coord: labelMargin, y_coord: 230, width: 100, height: boxHeight)
        placeLabel(inputText: " Hobbies:", x_coord: labelMargin, y_coord: 270, width: 100, height: boxHeight)
        placeLabel(inputText: " NetID:", x_coord: labelMargin, y_coord: 310, width: 100, height: boxHeight)
        placeLabel(inputText: " Best ProgLangs:", x_coord: labelMargin, y_coord: 350, width: 150, height: boxHeight)
        placeLabel(inputText: " Dept.:", x_coord: labelMargin, y_coord: 390, width: 100, height: boxHeight)
        placeLabel(inputText: " Team:", x_coord: labelMargin, y_coord: 430, width: 100, height: boxHeight)
        placeLabel(inputText: " Role:", x_coord: labelMargin, y_coord: 470, width: 100, height: boxHeight)
        
        //Info Text
        infoText = UILabel(frame: CGRect(x: 45, y: 610, width: 300, height: 120))
        infoText.textColor = UIColor.blue
        infoText.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoText.numberOfLines = 0
        infoText.textAlignment = .center
        infoText.layer.borderWidth = 2
        baseView.addSubview(infoText)
        
        //Image
        logo = UIImageView(image: UIImage(named: "Duke_University_logo"))
        logo.frame = CGRect(x: 205, y: 50, width: 150, height: 70)
        baseView.addSubview(logo)
        
        //Image
        avatar = UIImageView(image: UIImage(named: "avatar-male"))
        avatar.frame = CGRect(x: 10, y: 50, width: 100, height: 100)
        baseView.addSubview(avatar)
        loadEditInfo()
    }
    
    func loadEditInfo(){
        print(editedPerson.description)
        firstNameTextField.text = editedPerson.firstname
        lastNameTextField.text = editedPerson.lastname
        hometownTextField.text = editedPerson.wherefrom
        hobbyTextField.text = editedPerson.hobbies.joined(separator: ",")
        netIDTextField.text = editedPerson.netid
        netIDTextField.isEnabled = false
        bestProgLangTextField.text = editedPerson.languages.joined(separator: ",")
        departmentTextField.text = editedPerson.department
        teamNameTextField.text = editedPerson.team
        avatar.image = imageFromString(picString: editedPerson.picture)
        
        var row: Int = 0
        switch editedPerson.role{
        case .TA:
            row = 0
            roleSelected = .TA
        case .Professor:
            row = 1
            roleSelected = .Professor
        case .Student:
            row = 2
            roleSelected = .Student
        case .Other:
            row = 3
            roleSelected = .Other

        }
        roleSelector.selectRow(row, inComponent: 0, animated: true)
        
        switch editedPerson.gender{
        case .Male:
            selectedGender = .Male
            genderSelector.selectedSegmentIndex = 0
        case .Female:
            selectedGender = .Female
            genderSelector.selectedSegmentIndex = 1
        case .Other:
            selectedGender = .Other
            genderSelector.selectedSegmentIndex = 2
        case .Unknown:
            selectedGender = .Unknown
            genderSelector.selectedSegmentIndex = 3
        }
    }
    
    
    //Creates string from image
    func stringFromImage(_ imagePic: UIImage) -> String {
        let picImageData: Data = imagePic.jpegData(compressionQuality: 0.2) ?? Data()
        let picBase64 = picImageData.base64EncodedString()
        return picBase64
    }
    
    //Determines whether to add or update entry
    @objc func editButtonAction(_ sender: UIButton!) -> Bool{
        var newDatabase = [DukePerson]()
        var returnMessage = ""
        if lastNameTextField.text != "" && firstNameTextField.text != "" && netIDTextField.text != "" &&
            hobbyTextField.text != "" && hometownTextField.text != "" && bestProgLangTextField.text != "" &&
             departmentTextField.text != "" && teamNameTextField.text != "" {
            
            //Error checking for Phone number format
            if !stringToArray(hobbies: hobbyTextField.text!, languges: bestProgLangTextField.text!){
                 return false
            }
            
            let personCheck = createPerson()
            
            (returnMessage, newDatabase) = editDatabase(person: personCheck)
            dukeDatabase = newDatabase
            infoText.text = returnMessage
            infoText.textColor = UIColor.black
            shouldSave = true
            performSegue(withIdentifier: "editViewUnwind", sender: nil)
            return true
            
        }else{
            
            infoText.text = "Please fill in all of the fields of information"
            infoText.textColor = UIColor.red
            return false
        }
    }
    
    //This Button is used to cancel out of read-only view
    @objc func cancelButtonAction(_ sender: UIButton!){
        shouldSave = false
        performSegue(withIdentifier: "cancelUnwindCancel", sender: nil)
    }
    
    //This Button is used to clear the text fields
    @objc func clearButtonAction(_ sender: UIButton!){

        firstNameTextField.text?.removeAll()
        lastNameTextField.text?.removeAll()
        hobbyTextField.text?.removeAll()
        netIDTextField.text?.removeAll()
        bestProgLangTextField.text?.removeAll()
        hometownTextField.text?.removeAll()
        departmentTextField.text?.removeAll()
        teamNameTextField.text?.removeAll()
        infoText.text = "Cleared Info."
        infoText.textColor = UIColor.blue
        genderSelector.selectedSegmentIndex = -1
    }
    
    @objc func genderSelectorAction(_ sender: UISegmentedControl){
        switch genderSelector.selectedSegmentIndex{
        case 0:
            selectedGender = .Male
        case 1:
            selectedGender = .Female
        case 2:
            selectedGender = .Other
        case 3:
            selectedGender = .Unknown
        default:
            selectedGender = .Unknown
        }
    }
    
    //This function creates the role picker
    func createRolePicker(x_coord: Int, y_coord:Int, width:Int, height:Int){
        roleSelector = UIPickerView(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        roleSelector.delegate = self
        roleSelector.dataSource = self
        baseView.addSubview(roleSelector)
        roleSelected = .TA
    }

    //This function creates the gender selector
    func createGenderSelector(x_coord: Int, y_coord:Int, width:Int, height:Int){
        let genders = ["Male", "Female", "Other", "Unknown"]
        genderSelector = UISegmentedControl(items: genders)
        genderSelector.frame = CGRect(x: x_coord, y: y_coord, width: width, height:height)
        baseView.addSubview(genderSelector)
        genderSelector.addTarget(self, action: #selector(AddPersonViewController.genderSelectorAction(_:)), for: .valueChanged)
        genderSelector.selectedSegmentIndex = 3
        selectedGender = .Unknown
    }
    
    //This function creates labels
    func placeLabel(inputText:String, x_coord: Int, y_coord:Int, width:Int, height:Int){
        let label = UILabel(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        label.text = inputText
        label.textColor = UIColor.black
        baseView.addSubview(label)
    }
    
    //This function creates text fields
    func placeTextField(exampleText:String, x_coord: Int, y_coord:Int, width:Int, height:Int) -> UITextField{
        let textField = UITextField(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        textField.attributedPlaceholder = NSAttributedString(string: exampleText, attributes: [
            .foregroundColor: UIColor.darkGray])
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.borderWidth = 2
        baseView.addSubview(textField)
        return textField
    }
    
    //This function creates buttons
    func placeButton(name:String, x_coord: Int, y_coord:Int, width:Int, height:Int) -> UIButton{
        let button = UIButton(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        button.setTitle(name, for: .normal)
        button.backgroundColor = UIColor.blue
        button.layer.cornerRadius = 15
        button.setTitleColor(UIColor.black , for: .highlighted)
        baseView.addSubview(button)
        return button
    }
    
    //Verifies list of strings from hobby and language fields
    func stringToArray(hobbies: String, languges: String) -> Bool{
        let hobbyArray: [String] = hobbies.components(separatedBy: ",").filter({$0 != ""})
        let languagesArray: [String] = languges.components(separatedBy: ",").filter({$0 != ""})
        
        if hobbyArray.count > 3{
            infoText.text = "Hobbies Field allows up to three entries"
            infoText.textColor = UIColor.red
            return false
        }
        
        if languagesArray.count > 3{
            infoText.text = "Best Programming Languages allows up to three entries"
            infoText.textColor = UIColor.red
            return false
        }
        print(hobbyArray.description)
        print(languagesArray.description)
        
        
        hobbyList = hobbyArray
        programList = languagesArray
         return true
    }
    
    //This function creates a Dukeperson object using the text field info
    func createPerson() -> DukePerson{
        
        if editedPerson.picture == "" || editedPerson.picture == maleAvatar || editedPerson.picture == femaleAvatar{
            switch selectedGender{
            case .Male:
                editedPerson.picture = maleAvatar
            default:
                editedPerson.picture = femaleAvatar
            }
        }
        let person = DukePerson(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, homeTown: hometownTextField.text!, hobby: hobbyList, netID: netIDTextField.text!.lowercased(),  bestProgLang: programList, department: departmentTextField.text!, teamName: teamNameTextField.text!, gender: selectedGender.rawValue, role: roleSelected.rawValue, ID: editedPerson.id, degree: editedPerson.degree, picture: editedPerson.picture, email: editedPerson.email )
        return person
    }
}

//Needed for pickerView
extension EditViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roles.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return roles[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if roles[row] == "TA"{
            roleSelected = .TA
        }
        else if roles[row] == "Professor"{
            roleSelected = .Professor
        }
        else if roles[row] == "Student"{
            roleSelected = .Student
        }
        else {
            roleSelected = .Other
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if !shouldSave {
            return
        }else{
            if StorageClass.saveCohortInfo(dukeDatabase){
            }
        }
    }

}
