**HW4**
1. Enhanced search Bar that searches all fields
2. Added Update swiping action that sends a GET request to the ECE 564 server and only updates that individuals information
    - If the person hasn't POST any infomation to the server then nothing happens.
3. My progression indicator is a progression bar that activates during the refresh action and the update swipe action, after the information has loaded then the bar clears.
4. I send a PUT request to the server every time I edit my infomation.
5. Upon initial bootup of my app, if there is no data on disk it will load the default JSON file. It will also send a DELETE Request to the server to delete my current entry and then Send a POST Request to create a new entry with my default data.
    - The goal is to utilize all of the API methods in the server- PUT, POST, GET, and DELETE.


**HW3**
1. Add editViewController that allows for editing the selected DukePerson.
    - You find this view by clicking the edit button in the read-only view
2. Added Male and Female Avatars that change based off selected gender
    - Male Avatar for Male gender and Female Avatar for everything else
3. Customized UI
  



**HW 2**
1. Added launch screen
2. Added Error checking for Hobbies and Best languages strings
3. Added an App Icon that has full AppIcon Support
4. Added Save Button to allow for saving of data to Disk
5. Created a DukePersonJSON Struct to use for Decoding to help with decoding using ENUMs
    a. Created a DukePersonJSON to Dukeperson function converts the struct back to a class



**HW 1**

My homework is located in the master branch
Additional features are as followed:
1. Added a clear button that clears all of the fields, so the user doesn't have to delete each field manually when search for different people.
2. Added additional fields to the users information such as department and phone number.
3. Added a title using the imageview object and changed background color to something more professional.
4. When searching for people  you can use first name, last name, and netID. NetID will take precedence over the names. The find function will search for a matching netID first, and then search using the names if a netID isnt found. When searching using only names it will return the first match.
5. Created a information field box at the bottom of the app that displays personal information and error messages. Error messages will be displayed when incorrect formatting is detected in the fields and when there is a lack of information in the fields.
6. Added error checking for the phone number field that checks for formatting errors. The format should be 123-456-7890.
7. UI Customizations:
    - Buttons have rounded sides
    - Buttons text are hightlighted when pressed
    - Changed background color from white to light blue
    - Added a Duke logo title
    - added border to text field and increased border width
