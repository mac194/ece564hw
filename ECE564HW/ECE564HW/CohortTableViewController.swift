//
//  CohortTableViewController.swift
//  ECE564HW
//
//  Created by Loaner on 9/16/22.
//

import UIKit

class CohortTableViewController: UITableViewController, UISearchBarDelegate {
    var baseView: UIView!
    
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    private var observation: NSKeyValueObservation?

    deinit {
      observation?.invalidate()
    }

    
    var searching  = false
    
    var dukePeople = [DukePerson]()
    var arrayDukePeople: [[DukePerson]] = [[],[],[],[]]
    @IBOutlet weak var addPersonButton: UIBarButtonItem!
    var personSelected = DukePerson()
    var destVC: ReadOnlyViewController?
    var editVC: EditViewController?
    var readVC: ReadOnlyViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        loadInitialData()
        
        //Background color
        baseView = self.view
        baseView.backgroundColor = UIColor(red: 0.900, green: 0.900, blue: 1.0, alpha: 1.0)
        
        loadingView.backgroundColor = UIColor(red: 0.900, green: 0.900, blue: 1.0, alpha: 1.0)
        searchBar.placeholder = "Search Bar"
        progressBar.progress = 0.0
    }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        let blank = DukePerson()
        self.getServerInfo(dukie: blank)
        let secondsToDelay = 2.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + secondsToDelay) {
           self.loadArrayForTable(DB: dukeDatabase)
            if StorageClass.saveCohortInfo(dukeDatabase){
                self.tableView.reloadData()
            }
        }
        let secToDelay = 3.0
        DispatchQueue.main.asyncAfter(deadline: .now() + secToDelay) {
            self.progressBar.progress = 0.0
        }
    }
    
    func loadInitialData(){
        if let dukeData = StorageClass.loadJSONFile(){
            dukePeople = dukeData
            dukeDatabase = dukeData
        } else{
            guard let path = Bundle.main.path(forResource: "ECE564Cohort", ofType: "json") else {
                return
            }
            let url = URL(fileURLWithPath: path)
            do{
                let jsonData = try Data(contentsOf: url)
               
                let decoded = try JSONDecoder().decode([DukePersonJSON].self, from: jsonData)
                dukePeople.removeAll()
                for person in decoded{
                    let p = structToClass(structJSON: person)
                    //Update Picture
                    if p.netid == "mac194"{
                        dukePeople.append(marcus)
                        deleteServerInfo(persons: marcus)
                        postServerInfo(persons: marcus)
                        continue
                    }else if p.gender == .Male{
                        p.picture = maleAvatar
                    } else{
                        p.picture = femaleAvatar
                    }
                    dukePeople.append(p)
                }
                dukeDatabase = dukePeople
                
                if StorageClass.saveCohortInfo(dukePeople){
                    print("Loaded and saved Default Data!")
                   
                } else{
                    print("Unable to saved Default Data!")
                }
            }catch{
                print("Error: \(error)")
                }
          }
        loadArrayForTable(DB: dukePeople)
    }
    
    
    
    //Creates string from image
    func stringFromImage(_ imagePic: UIImage) -> String {
        let picImageData: Data = imagePic.jpegData(compressionQuality: 0.2) ?? Data()
        let picBase64 = picImageData.base64EncodedString()
        return picBase64
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.arrayDukePeople.count
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrayDukePeople[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CohortListProtoCell", for: indexPath)
        let nextPerson: DukePerson = arrayDukePeople[indexPath.section][indexPath.row]
        cell.detailTextLabel?.text = nextPerson.description
        cell.textLabel?.text = nextPerson.firstname + " " + nextPerson.lastname
        cell.imageView?.image = imageFromString(picString: nextPerson.picture)
        cell.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.9, alpha: 1.0)
        cell.layer.borderWidth = 1
        cell.layer.borderColor = CGColor(srgbRed: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
        }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return "Professor(s)"
        case 1:
            return "TA(s)"
        case 2:
            return "Student(s)"
        default:
            return "Other(s)"
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
            header.textLabel?.textColor = UIColor.black
            header.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            header.textLabel?.frame = header.bounds
            header.textLabel?.textAlignment = .center
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellHeight:CGFloat = CGFloat()
        if indexPath.row % 2 == 0 {
            cellHeight = 120       }
        else if indexPath.row % 2 != 0 {
            cellHeight = 120
        }
        return cellHeight
    }
    
  
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            destVC!.personViewed = arrayDukePeople[indexPath.section][indexPath.row]
    }
    
    // MARK: - TableView SwipeActions
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let updateAction = UIContextualAction(style: .normal, title: "UPDATE") { (action, view, completionHandler) in
            let person = self.arrayDukePeople[indexPath.section][indexPath.row]
            
            self.getServerInfoOne(dukie: person)
            let secondsToDelay = 1.5
            DispatchQueue.main.asyncAfter(deadline: .now() + secondsToDelay) {
                if StorageClass.saveCohortInfo(dukeDatabase){
                    
                    self.loadArrayForTable(DB: dukeDatabase)
                    self.tableView.reloadData()
                    }
            }
            let secToDelay = 3.0
            DispatchQueue.main.asyncAfter(deadline: .now() + secToDelay) {
                self.progressBar.progress = 0.0
            }
            completionHandler(true)
        }
        return UISwipeActionsConfiguration(actions: [updateAction])
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "DELETE") { (action, view, completionHandler) in
            
            for (index,dukie) in dukeDatabase.enumerated(){
                if dukie.netid.lowercased() == self.arrayDukePeople[indexPath.section][indexPath.row].netid.lowercased(){
                    dukeDatabase.remove(at: index)
                }
            }
            
            self.arrayDukePeople[indexPath.section].remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            if StorageClass.saveCohortInfo(dukeDatabase){
            completionHandler(true)
            }
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction])//, editAction])
    }
    // MARK: - Table view return and leave section
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "readOnlyInfo" {
            destVC = segue.destination as? ReadOnlyViewController
        }
    }
    // MARK: - Unwind Stuff
    @IBAction func unwindFromAddPerson(segue: UIStoryboardSegue) {
        let source: AddPersonViewController = segue.source as! AddPersonViewController
        let newDukeArray = source.returnedPerson
        searchBar.text = ""
        loadArrayForTable(DB: newDukeArray)
        self.tableView.reloadData()
    }
    
    @IBAction func unwindFromReadOnly(segue: UIStoryboardSegue) {
        loadArrayForTable(DB: dukeDatabase)
        searchBar.text = ""
        self.tableView.reloadData()
    }
    
    func loadArrayForTable(DB: [DukePerson]){
        arrayDukePeople.removeAll()
        arrayDukePeople = [[],[],[],[]]
        for dukie in DB{
            if dukie.role == .Professor{
                arrayDukePeople[0].append(dukie)
            } else if dukie.role == .TA{
                arrayDukePeople[1].append(dukie)
            } else if dukie.role == .Student{
                arrayDukePeople[2].append(dukie)
            } else{
                arrayDukePeople[3].append(dukie)
            }
        }
    }
    
    // MARK: - Search Bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var filterData: [DukePerson] = []
        if searchText.count == 0 {
            
            loadArrayForTable(DB: dukeDatabase)
            
        } else {
            for (personSection) in arrayDukePeople{
                for (persons) in personSection{
                    if persons.description.lowercased().contains(searchText.lowercased()) ||
                        persons.role.rawValue.lowercased().contains(searchText.lowercased()) ||
                        persons.gender.rawValue.lowercased().contains(searchText.lowercased())
                        {
                        filterData.append(persons)
                    }
                }
            }
            loadArrayForTable(DB: filterData)
        }
        self.tableView.reloadData()
    }
    
    
    // MARK: - HTTP Stuff
    //Send PUT Request to Server
    func putServerInfo(persons: DukePerson){
        
        guard let url = URL(string: "https://ece564server-vapor.colab.duke.edu/entries/mac194") else{
            return
        }
        var request = URLRequest(url: url)
        
        request.httpMethod = "PUT"
        let username = "mac194"
        let password = "132ce9ce1bb0d9fb2edeecc86bfa315fa5994d95ce5720796bd3963c3add615c"
        let loginString = "\(username):\(password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else{
            return
        }
        let base64LoginData = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonDict = [
            "firstname":persons.firstname as Any,
            "lastname": persons.lastname as Any,
            "netid": persons.netid as Any,
            "wherefrom": persons.wherefrom as Any,
            "gender": persons.gender.rawValue as Any,
            "role": persons.role.rawValue as Any,
            "degree": persons.degree as Any,
            "team": persons.team as Any,
            "hobbies": persons.hobbies as Any,
            "languages": persons.languages as Any,
            "department": persons.department as Any,
            "email": persons.email as Any,
            "picture": persons.picture as Any
        ]
        request.httpBody = try? JSONSerialization.data(withJSONObject: jsonDict, options: .fragmentsAllowed)
        
        let task = URLSession.shared.dataTask(with: request) { data, repsonse, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print(response)
            }
            catch{
                print(error)
            }
        }
        task.resume()
    }
    
    //Send POST Request to Server
    func postServerInfo(persons: DukePerson){
        
        guard let url = URL(string: "https://ece564server-vapor.colab.duke.edu/entries/create") else{
            return
        }
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        let username = "mac194"
        let password = "132ce9ce1bb0d9fb2edeecc86bfa315fa5994d95ce5720796bd3963c3add615c"
        let loginString = "\(username):\(password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else{
            return
        }
        let base64LoginData = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonDict = [
            "firstname":persons.firstname as Any,
            "lastname": persons.lastname as Any,
            "netid": persons.netid as Any,
            "wherefrom": persons.wherefrom as Any,
            "gender": persons.gender.rawValue as Any,
            "role": persons.role.rawValue as Any,
            "degree": persons.degree as Any,
            "team": persons.team as Any,
            "hobbies": persons.hobbies as Any,
            "languages": persons.languages as Any,
            "department": persons.department as Any,
            "email": persons.email as Any,
            "picture": persons.picture as Any
        ]
        request.httpBody = try? JSONSerialization.data(withJSONObject: jsonDict, options: .fragmentsAllowed)
        
        let task = URLSession.shared.dataTask(with: request) { data, repsonse, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print(response)
            }
            catch{
                print(error)
            }
        }
        task.resume()
    }
    
    //Send DELETE Request to Server
    func deleteServerInfo(persons: DukePerson){
        
        guard let url = URL(string: "https://ece564server-vapor.colab.duke.edu/entries/mac194") else{
            return
        }
        var request = URLRequest(url: url)
        
        request.httpMethod = "DELETE"
        let username = "mac194"
        let password = "132ce9ce1bb0d9fb2edeecc86bfa315fa5994d95ce5720796bd3963c3add615c"
        let loginString = "\(username):\(password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else{
            return
        }
        let base64LoginData = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonDict = [
            "firstname":persons.firstname as Any,
            "lastname": persons.lastname as Any,
            "netid": persons.netid as Any,
            "wherefrom": persons.wherefrom as Any,
            "gender": persons.gender.rawValue as Any,
            "role": persons.role.rawValue as Any,
            "degree": persons.degree as Any,
            "team": persons.team as Any,
            "hobbies": persons.hobbies as Any,
            "languages": persons.languages as Any,
            "department": persons.department as Any,
            "email": persons.email as Any,
            "picture": persons.picture as Any
        ]
        request.httpBody = try? JSONSerialization.data(withJSONObject: jsonDict, options: .fragmentsAllowed)
        
        let task = URLSession.shared.dataTask(with: request) { data, repsonse, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print(response)
            }
            catch{
                print(error)
            }
        }
        task.resume()
    }
    
    //Sends GET request to Server for all entries
    func getServerInfo(dukie: DukePerson){
        
        var dukePersons = [DukePerson]()
        var found  = false
        
        guard let url = URL(string: "https://ece564server-vapor.colab.duke.edu/entries/all") else{
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let username = "mac194"
        let password = "132ce9ce1bb0d9fb2edeecc86bfa315fa5994d95ce5720796bd3963c3add615c"
        let loginString = "\(username):\(password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else{
            return
        }
        let base64LoginData = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let serverInfo = try JSONDecoder().decode([DukePersonJSON].self, from: data)
                print("RESPONSE: \(serverInfo)")
                
                for personGET in serverInfo{
                    if dukie.netid == "netid"{
                        found = true
                        let p = structToClass(structJSON: personGET)
                        dukePersons.append(p)
                    }
                }
                if found {
                    dukeDatabase = dukePersons
                }
            }
            catch{
                print(error)
            }
        }
        self.observation = task.progress.observe(\.fractionCompleted) { progress, _ in
                DispatchQueue.main.async {
                self.progressBar.progress = Float(progress.fractionCompleted)
                print("progress: ", progress.fractionCompleted)
                }
            }
        task.resume()
    }
    
    //GET one person from Server
    func getServerInfoOne(dukie: DukePerson){
        print(dukie.netid.lowercased())
        guard let url = URL(string: "https://ece564server-vapor.colab.duke.edu/entries/\(dukie.netid.lowercased())") else{
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let username = "mac194"
        let password = "132ce9ce1bb0d9fb2edeecc86bfa315fa5994d95ce5720796bd3963c3add615c"
        let loginString = "\(username):\(password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else{
            return
        }
        let base64LoginData = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                print("PRINT: \(String(decoding:data, as: UTF8.self))")
                print(response as Any)
                let serverInfo = try JSONDecoder().decode(DukePersonJSON.self, from: data)
                print("RESPONSE: \(serverInfo)")
                let p = structToClass(structJSON: serverInfo)

                for (index, person) in dukeDatabase.enumerated(){
                    if p.netid == person.netid{
                        dukeDatabase.remove(at: index)
                        dukeDatabase.append(p)
                        break
                    }
                }
            }
            catch{
                print(error)
            }
        }
        self.observation = task.progress.observe(\.fractionCompleted) { progress, _ in
                DispatchQueue.main.async {
                self.progressBar.progress = Float(progress.fractionCompleted)
                print("progress: ", progress.fractionCompleted)
                }
            }
        task.resume()
    }
}


