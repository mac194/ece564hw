//
//  DataModel.swift
//  ECE564HW
//
//  Created by mac194 on 9/2/22.
//

import UIKit

struct DukePersonJSON: Codable{
    var netid = "netID"
    var languages: [String] = []
    var department = "ECE"
    var team = "NA"
    var role = "Student"
    var id = "ID"
    var degree = "MS"
    var picture = "pic"
    var email = "yahoo"
    var firstname = "first"
    var lastname = "last"
    var gender = "Male"
    var wherefrom = "location"
    var hobbies: [String] = []
}

enum Gender: String, Codable {
    case Male = "Male"
    case Female = "Female"
    case Other = "Other"
    case Unknown = "Unknown"
}

enum Role: String, Codable{
    case TA = "TA"
    case Professor = "Professor"
    case Student = "Student"
    case Other = "Other"
}

class Person: Codable{
    var firstname = "first"
    var lastname = "last"
    var gender: Gender = .Unknown
    var wherefrom = "location"
    var hobbies: [String] = []

    enum CodingKeys: String, CodingKey{
        case firstname
        case lastname
        case wherefrom
        case gender
        case hobbies
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.firstname, forKey: .firstname)
        try container.encode(self.lastname, forKey: .lastname)
        try container.encode(self.wherefrom, forKey: .wherefrom)
        try container.encode(self.gender, forKey: .gender)
        try container.encode(self.hobbies, forKey: .hobbies)
    }
}

class DukePerson: Person, CustomStringConvertible{
    var netid = "netid"
    var languages: [String] = []
    var department = "ECE"
    var team = "NA"
    var role: Role = .TA
    var id = "ID"
    var degree = "MS"
    var picture = "pic"
    var email = "yahoo"
    var description: String{
        return "My name is \(firstname) \(lastname) and I am from \(wherefrom). My best programming language is \(languages.joined(separator: ", ")), and my favorite hobby is \(hobbies.joined(separator: ", ")). You can reach me at \(netid)@duke.edu"
    }
    convenience init(firstName: String, lastName: String, homeTown: String, hobby: [String], netID: String, bestProgLang: [String], department: String, teamName: String, gender: String, role: String, ID:String, degree: String, picture: String, email: String) {
        self.init()
        self.firstname = firstName
        self.lastname = lastName
        self.wherefrom = homeTown
        self.hobbies = hobby
        self.netid = netID
        self.languages = bestProgLang
        self.department = department
        self.team = teamName
        
        switch role{
        case "TA":
            self.role = .TA
        case "Professor":
            self.role = .Professor
        case "Student":
            self.role = .Student
        case "Other":
            self.role = .Other
        default:
            self.role = .Other
        }
        
        switch gender{
        case "Male":
            self.gender = .Male
        case "Female":
            self.gender = .Female
        case "Other":
            self.gender = .Other
        case "Unknown":
            self.gender = .Unknown
        default:
            self.gender = .Unknown
        }
        
        self.id = ID
        self.degree = degree
        self.picture = picture
        self.email = email
    }
    
  
    enum CodingKeys: String, CodingKey{
        case firstname = "firstname"
        case lastname = "lastname"
        case wherefrom = "wherefrom"
        case gender = "gender"
        case hobbies = "hobbies"
        case email = "email"
        case id = "id"
        case role = "role"
        case department = "department"
        case picture = "picture"
        case languages = "languages"
        case netid = "netid"
        case team = "team"
        case degree = "degree"
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstname, forKey: .firstname)
        try container.encode(lastname, forKey: .lastname)
        try container.encode(wherefrom, forKey: .wherefrom)
        try container.encode(gender, forKey: .gender)
        try container.encode(hobbies, forKey: .hobbies)
        try container.encode(email, forKey: .email)
        try container.encode(id, forKey: .id)
        try container.encode(role, forKey: .role)
        try container.encode(department, forKey: .department)
        try container.encode(picture, forKey: .picture)
        try container.encode(languages, forKey: .languages)
        try container.encode(netid, forKey: .netid)
        try container.encode(team, forKey: .team)
        try container.encode(degree, forKey: .degree)
    }
}

var dukeDatabase: [DukePerson] = []
let macPic = stringFromImage(UIImage(named: "small")!)
let maleAvatar = stringFromImage(UIImage(named: "avatar-male")!)
let femaleAvatar = stringFromImage(UIImage(named: "female_avatar")!)

func stringFromImage(_ imagePic: UIImage) -> String {
    let picImageData: Data = imagePic.jpegData(compressionQuality: 0.2)!
    let picBase64 = picImageData.base64EncodedString()
    return picBase64
}

func resizeImage(image: UIImage, width: CGFloat) -> UIImage {
    let scale = width / image.size.width
    let height = image.size.height * scale
    UIGraphicsBeginImageContext(CGSize(width: width, height: height))
    image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
    let adjustedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return adjustedImage!
}

//Create Image from string
func imageFromString(picString: String) -> UIImage{
    guard let pic = Data(base64Encoded: picString), let image = UIImage(data: pic)
    else {
        print("Error: Can't create image!")
         return UIImage(named: "avatar-male")!
    }
    return resizeImage(image: image, width: 120.0)
}

//loading the Database with default entries
let marcus = DukePerson(firstName: "Marcus", lastName: "Cox", homeTown: "Charleston,SC", hobby: ["Weight Lifting", "Video Games", "Cooking"], netID: "mac194", bestProgLang: ["C++", "Python", "Swift"] , department: "ECE", teamName: "NA", gender: "Male", role: "Student", ID: "NA", degree: "Meng", picture: macPic, email: "mac194@duke.edu")
let professor = DukePerson(firstName: "Ric", lastName: "Telford", homeTown: "Chatham County, NC", hobby: ["golf", "swimming", "Hiking"], netID: "rt113", bestProgLang: ["Swift", "C", "C++"], department: "ECE", teamName: "NA", gender: "Male", role: "Professor", ID: "NA", degree: "PhD", picture: "", email: "")

func defaultPeople(){
    print(addOrUpdateDatabase(person: marcus))
    print(addOrUpdateDatabase(person: professor))
}

//This function is used to add and update Database
func addOrUpdateDatabase(person: DukePerson) -> (String, [DukePerson]){
    
    for personCheck in dukeDatabase{
        if person.netid.lowercased() == personCheck.netid.lowercased(){
            return ("This person already exist", dukeDatabase)
        }
    }
    dukeDatabase.append(person)
    return ("The person has been added!", dukeDatabase)
}

//This function is used to add and update Database
func editDatabase(person: DukePerson) -> (String, [DukePerson]){
    for (index, personCheck) in dukeDatabase.enumerated(){
        if person.netid.lowercased() == personCheck.netid.lowercased(){
            dukeDatabase.remove(at: index)
        }
    }
    if person.netid.lowercased() == "mac194"{
      
        let post = CohortTableViewController()
        post.putServerInfo(persons: person)
    }
    dukeDatabase.append(person)
    return ("The person's info has been edited!", dukeDatabase)
}

// This function is used to find entries and display their information
func find(person: DukePerson) -> (String, DukePerson){
    
    //print(dukeDatabase.description)
    for personCheck in dukeDatabase{
        if (personCheck.netid.lowercased() == person.netid.lowercased()){
            
            return (personCheck.description, personCheck)
        }
    }
    for personCheck in dukeDatabase{
        if (personCheck.firstname.lowercased() == person.firstname.lowercased() && personCheck.lastname.lowercased() == person.lastname.lowercased()){
            
            return (personCheck.description, personCheck)
        }
    }
    return ("The person was not found!", person)
}


func structToClass(structJSON: DukePersonJSON) -> DukePerson{
    //Need to add error checking for Hobbies and Languages
    let transfer = DukePerson(firstName: structJSON.firstname, lastName: structJSON.lastname, homeTown: structJSON.wherefrom, hobby: structJSON.hobbies, netID: structJSON.netid, bestProgLang: structJSON.languages , department: structJSON.department, teamName: structJSON.team, gender: structJSON.gender, role: structJSON.role, ID: structJSON.id, degree: structJSON.degree, picture: structJSON.picture, email: structJSON.email)
   
    return transfer
}

//may not need this
func classToStruct( dukeClass: DukePerson) -> DukePersonJSON{
    var transfer = DukePersonJSON()
    transfer.firstname = dukeClass.firstname
    transfer.lastname = dukeClass.lastname
    transfer.wherefrom = dukeClass.wherefrom
    transfer.hobbies = dukeClass.hobbies
    transfer.netid = dukeClass.netid
    
    switch dukeClass.gender{
    case .Male:
        transfer.gender = "Male"
    case .Female:
        transfer.gender = "Female"
    case .Other:
        transfer.gender = "Other"
    case .Unknown:
        transfer.gender = "Unknown"
    }
    
    switch dukeClass.role{
    case .TA:
        transfer.role = "TA"
    case .Professor:
        transfer.role = "Professor"
    case .Student:
        transfer.role = "Student"
    case .Other:
        transfer.role = "Other"
    }
    transfer.id = dukeClass.id
    transfer.email = dukeClass.email
    transfer.picture = dukeClass.picture
    transfer.degree = dukeClass.degree
    transfer.department = dukeClass.department
    transfer.languages = dukeClass.languages
    transfer.team = dukeClass.team
    
    return transfer
    
}

