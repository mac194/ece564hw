//
//  ReadOnlyViewController.swift
//  ECE564HW
//
//  Created by Loaner on 9/16/22.
//

import UIKit

class ReadOnlyViewController: UIViewController {

    var baseView: UIView!
    
    
    var firstNameTextField: UITextField!
    var lastNameTextField: UITextField!
    var hometownTextField: UITextField!
    var hobbyTextField: UITextField!
    var netIDTextField: UITextField!
    var bestProgLangTextField: UITextField!
    var departmentTextField: UITextField!
    var infoText: UILabel!
    var teamNameTextField: UITextField!
    
    var genderSelector: UISegmentedControl!
    var roleSelector: UIPickerView!

    var roles = ["TA", "Professor", "Student", "Other"]
    
    
    var hobbyList: [String] = []
    var programList: [String] = []
    
    var logo: UIImageView!
    var avatar: UIImageView!
    
    var personViewed = DukePerson()

    override func viewDidLoad() {
        let labelMargin:Int = 10
        let textMargin:Int = 180
        let boxHeight:Int = 35
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        baseView = self.view
        baseView.backgroundColor = UIColor(red: 0.900, green: 0.900, blue: 1.0, alpha: 1.0)
        
        //UI SegmentContol Setup
        createGenderSelector(x_coord: 45, y_coord: 575, width: 300, height: 50)
        
        //UIpickerView Setup
        createRolePicker(x_coord: textMargin - 10, y_coord: 525, width: 220, height: boxHeight + 10)
       
        //Text Field Setup
        firstNameTextField =  placeTextField(exampleText: " ex. Jane", x_coord: textMargin, y_coord: 210, width: 200, height: boxHeight)
        lastNameTextField =  placeTextField(exampleText: " ex. Doe", x_coord: textMargin, y_coord: 250, width: 200, height: boxHeight)
        hometownTextField =  placeTextField(exampleText: " ex. Durham,NC", x_coord: textMargin, y_coord: 290, width: 200, height: boxHeight)
        hobbyTextField = placeTextField(exampleText: " ex. Hiking, Biking, Golfing", x_coord: textMargin, y_coord: 330, width: 200, height: boxHeight)
        netIDTextField = placeTextField(exampleText: " ex. jd123", x_coord: textMargin, y_coord: 370, width: 200, height: boxHeight)
        bestProgLangTextField = placeTextField(exampleText: " ex. Python, C, C++", x_coord: textMargin, y_coord: 410, width: 200, height: boxHeight)
        departmentTextField = placeTextField(exampleText: " ex. ECE", x_coord: textMargin, y_coord: 450, width: 200, height: boxHeight)
        teamNameTextField = placeTextField(exampleText: " ex. Team ECE", x_coord: textMargin, y_coord: 490, width: 200, height: boxHeight)
        
        
        //Label Setup
        placeLabel(inputText: " First Name:", x_coord: labelMargin, y_coord: 210, width: 100, height: boxHeight)
        placeLabel(inputText: " Last Name:", x_coord: labelMargin, y_coord: 250, width: 100, height: boxHeight)
        placeLabel(inputText: " From:", x_coord: labelMargin, y_coord: 290, width: 100, height: boxHeight)
        placeLabel(inputText: " Hobbies:", x_coord: labelMargin, y_coord: 330, width: 100, height: boxHeight)
        placeLabel(inputText: " NetID:", x_coord: labelMargin, y_coord: 370, width: 100, height: boxHeight)
        placeLabel(inputText: " Best ProgLangs:", x_coord: labelMargin, y_coord: 410, width: 150, height: boxHeight)
        placeLabel(inputText: " Dept.:", x_coord: labelMargin, y_coord: 450, width: 100, height: boxHeight)
        placeLabel(inputText: " Team:", x_coord: labelMargin, y_coord: 490, width: 100, height: boxHeight)
        placeLabel(inputText: " Role:", x_coord: labelMargin, y_coord: 525, width: 100, height: boxHeight)
        
        //Info Text
        infoText = UILabel(frame: CGRect(x: 45, y: 630, width: 300, height: 150))
        infoText.textColor = UIColor.blue
        infoText.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoText.numberOfLines = 0
        infoText.textAlignment = .center
        infoText.layer.borderWidth = 2
        baseView.addSubview(infoText)
        
        //Image
        logo = UIImageView(image: UIImage(named: "Duke_University_logo"))
        logo.frame = CGRect(x: 205, y: 110, width: 150, height: 70)
        baseView.addSubview(logo)
        
        //Image
        avatar = UIImageView(image: UIImage(named: "avatar-male"))
        avatar.frame = CGRect(x: 10, y: 110, width: 100, height: 100)
        baseView.addSubview(avatar)
        
        //Load Read Only Info
        loadReadOnlyInfo()
    }
    
    func loadReadOnlyInfo(){
        roles.removeAll()
        roles.append(personViewed.role.rawValue)
        print(personViewed.description)
        firstNameTextField.text = personViewed.firstname
        lastNameTextField.text = personViewed.lastname
        hometownTextField.text = personViewed.wherefrom
        hobbyTextField.text = personViewed.hobbies.joined(separator: ",")
        netIDTextField.text = personViewed.netid
        bestProgLangTextField.text = personViewed.languages.joined(separator: ",")
        departmentTextField.text = personViewed.department
        teamNameTextField.text = personViewed.team
        avatar.image = imageFromString(picString: personViewed.picture)
        infoText.text = personViewed.description
        
        switch personViewed.gender{
        case .Male:
            genderSelector.selectedSegmentIndex = 0
        case .Female:
            genderSelector.selectedSegmentIndex = 1
        case .Other:
            genderSelector.selectedSegmentIndex = 2
        case .Unknown:
            genderSelector.selectedSegmentIndex = 3
        }
    }
    
    
    //Creates string from image
    func stringFromImage(_ imagePic: UIImage) -> String {
        let picImageData: Data = imagePic.jpegData(compressionQuality: 0.2) ?? Data()
        let picBase64 = picImageData.base64EncodedString()
        return picBase64
    }
    
    //This function creates the role picker
    func createRolePicker(x_coord: Int, y_coord:Int, width:Int, height:Int){
        roleSelector = UIPickerView(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        roleSelector.delegate = self
        roleSelector.dataSource = self
        baseView.addSubview(roleSelector)
    }

    //This function creates the gender selector
    func createGenderSelector(x_coord: Int, y_coord:Int, width:Int, height:Int){
        let genders = ["Male", "Female", "Other", "Unknown"]
        genderSelector = UISegmentedControl(items: genders)
        genderSelector.frame = CGRect(x: x_coord, y: y_coord, width: width, height:height)
        genderSelector.isEnabled = false
        baseView.addSubview(genderSelector)
        genderSelector.addTarget(self, action: #selector(AddPersonViewController.genderSelectorAction(_:)), for: .valueChanged)
        genderSelector.selectedSegmentIndex = 3
    }
    
    //This function creates labels
    func placeLabel(inputText:String, x_coord: Int, y_coord:Int, width:Int, height:Int){
        let label = UILabel(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        label.text = inputText
        label.textColor = UIColor.black
        baseView.addSubview(label)
    }
    
    //This function creates text fields
    func placeTextField(exampleText:String, x_coord: Int, y_coord:Int, width:Int, height:Int) -> UITextField{
        let textField = UITextField(frame: CGRect(x: x_coord, y: y_coord, width: width, height: height))
        textField.attributedPlaceholder = NSAttributedString(string: exampleText, attributes: [
            .foregroundColor: UIColor.darkGray])
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.borderWidth = 2
        textField.isEnabled = false
        baseView.addSubview(textField)
        return textField
    }
    
    //Verifies list of strings from hobby and language fields
    func stringToArray(hobbies: String, languges: String) -> Bool{
        let hobbyArray: [String] = hobbies.components(separatedBy: ",").filter({$0 != ""})
        let languagesArray: [String] = languges.components(separatedBy: ",").filter({$0 != ""})
        
        if hobbyArray.count > 3{
            infoText.text = "Hobbies Field allows up to three entries"
            infoText.textColor = UIColor.red
            return false
        }
        
        if languagesArray.count > 3{
            infoText.text = "Best Programming Languages allows up to three entries"
            infoText.textColor = UIColor.red
            return false
        }
        print(hobbyArray.description)
        print(languagesArray.description)
        
        hobbyList = hobbyArray
        programList = languagesArray
         return true
    }
}

//Needed for pickerView
extension ReadOnlyViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roles.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return roles[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editInfoView"{
            let destVC = segue.destination as! EditViewController
            print(personViewed)
            destVC.editedPerson = personViewed
            print(destVC.editedPerson.description)
        }
    }
    
    @IBAction func unwindFromEditView(segue: UIStoryboardSegue) {
        if segue.identifier == "editViewUnwind"{
        performSegue(withIdentifier: "UnwindReadOnly", sender: nil)
        }
    }
}
