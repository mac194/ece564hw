//
//  StorageFile.swift
//  ECE564HW
//
//  Created by Loaner on 9/11/22.
//

import UIKit

class StorageClass: NSObject, Codable{
    
    static let documentsDir = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let JsonURL = documentsDir.appendingPathComponent("ECE564Cohort")
    
    override init() {
        super.init()
    }
    
   
    static func loadJSONFile() -> [DukePerson]?{
        let decoder = JSONDecoder()
        var dukePersons = [DukePerson]()
        let tempData: Data
        
        do {
            print("Loaded Loaction: \(JsonURL)")
            tempData = try Data(contentsOf: JsonURL)
        } catch let error as NSError {
            print(error)
            return nil
        }
        do{
            let decoded = try decoder.decode([DukePersonJSON].self, from: tempData)
            for person in decoded{
                let p = structToClass(structJSON: person)
                dukePersons.append(p)
            }
            
        } catch let error as NSError{
            print(error)
        }
        
        return dukePersons
    }
    static func saveCohortInfo(_ dukePeople: [DukePerson]) -> Bool {
        var outputData = Data()
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(dukePeople) {
            if let json = String(data: encoded, encoding: .utf8) {
                print(json)
                outputData = encoded
            }
            else { return false }
            
            do {
                    print("Saved Loaction: \(JsonURL)")
                    try outputData.write(to: JsonURL)
            } catch let error as NSError {
                print (error)
                return false
            }
            
            return true
        }
        else { return false }
    }

}



